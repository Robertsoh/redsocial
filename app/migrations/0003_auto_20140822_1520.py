# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0002_auto_20140822_1515'),
    ]

    operations = [
        migrations.RenameField(
            model_name='account',
            old_name='created',
            new_name='created_at',
        ),
        migrations.RenameField(
            model_name='account',
            old_name='modified',
            new_name='updated_at',
        ),
    ]
