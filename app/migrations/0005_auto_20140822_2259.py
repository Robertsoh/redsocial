# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0004_auto_20140822_1839'),
    ]

    operations = [
        migrations.RenameField(
            model_name='friend',
            old_name='ending_account_id',
            new_name='ending_account',
        ),
        migrations.RenameField(
            model_name='friend',
            old_name='starting_account_id',
            new_name='starting_account',
        ),
    ]
