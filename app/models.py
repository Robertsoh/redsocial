from django.db import models
from django.contrib import admin
# Create your models here.
class BaseModel(models.Model):

    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        abstract = True

class Account(BaseModel):
    name = models.CharField(max_length=50)
    
   
class Friend(BaseModel):
    starting_account_id = models.ForeignKey(Account)
    ending_account_id = models.IntegerField()

