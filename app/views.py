from django.shortcuts import render
from app.models import  Account, Friend
import json
from django.http import JsonResponse, HttpResponse
from django.views.generic import TemplateView, View


#from django.shortcuts import get_object_or_404
# Create your views here. 

class MainView(View):
    def get(self, request, *args, **kwargs):
      
        data = {
            "account_collection_url": "http://localhost:8000/app/accounts",
            "account_url": "http://localhost:8000/app/accounts/{account_id}",
            "friend_collection_url": "http://localhost:8000/app/accounts/{account_id}/friends",
            "mutual_friends_url": "http://localhost:8000/app/accounts/{account_id}/mutualfriends/{friend_account_id}",
            "add_friendship": "http://localhost:8000/app/add_friend",
            "remove_friendship": "http://localhost:8000/app/remove_friend",
            "stats_url": "http://localhost:8000/app/stats"
        }
            
        return JsonResponse(data)

class AccountsView(View):
    

    def get(self, request, *args, **kwargs):
        accounts = []        
        for account in Account.objects.all():
            accounts.append({
                'account_id': account.id,
                'name': account.name,
                'url': 'http://localhost:8000/app/accounts/'+str(account.id)
            })    
        return JsonResponse(accounts, safe=False)

    def post(self, request, *args, **kwargs):

        data = json.loads(request.body)
        account = Account.objects.create(
            name = data['name']            
        )  
        response = HttpResponse(status=201)  
        
        return response


class AccountDetailsView(View):

    def get(self, request, *args, **kwargs):
        id=kwargs.get('id', None)
        #id = request.GET.get('id', None)       
        account = Account.objects.get(id=id)
        data = {
            'account_id': account.id,
            'name': account.name,
            'created_at': account.created_at,
            'updated_at': account.updated_at,
            'friend_collection_url': 'http://localhost:8000/app/accounts/'+str(account.id)+'/friends'
        }           
        return JsonResponse(data)



class AddFriendView(View):


    def post(self, request, *args, **kwargs):

        data = json.loads(request.body)
        account = Friend.objects.create(
            starting_account_id = data['starting_account_id']  
            ending_account_id =  data['ending_account_id']          
        )  
        response = HttpResponse(status=201)  
        
        return response


"""
class DeleteFriendView(View):

    def post(self, request, *args, **kwargs):
        data = json.loads(request.body)        
        return JsonResponse(data)"""


class FriendsView(View):
    def get(self, request, *args, **kwargs):
        friendsList = []
        id=kwargs.get('id', None)
        for friend in Friend.objects.filter(starting_account_id=id):
            account = Account.objects.get(id=friend.ending_account_id)
            friendsList.append({
                'account_id': account.id,
                'name': account.name,
                'url': 'http://localhost:8000/app/accounts/'+str(account.id)
                
            })      
        return JsonResponse(friendsList, safe = False)

"""class ListFriendsCommonView(view):
    def get(self, request, *args, **kwargs):
        friendsList = []
        id = request.GET.get('id', None)


        for friend in Friend.objects.get(starting_account_id=id):
            account = Account.objects.get(id=id)
            friendsList.append({
                'id': account.id,
                'name': account.name
                
            })
      
        return JsonResponse(friendsList)"""