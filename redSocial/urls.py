from django.conf.urls import patterns, include, url
from django.contrib import admin
from app import views

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'redSocial.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', views.MainView.as_view(), name='main'),
    url(r'^app/accounts/$', views.AccountsView.as_view(), name='account_collection_url'),
    url(r'^app/accounts/(?P<id>\d+)/$', views.AccountDetailsView.as_view(), name='account_url'),
    url(r'^app/accounts/(?P<id>\d+)/friends/$', views.FriendsView.as_view(), name='friend_collection_url'),
    url(r'^app/add_friend/$', views.AddFriendView.as_view(), name='add_friendship'),

    
)
